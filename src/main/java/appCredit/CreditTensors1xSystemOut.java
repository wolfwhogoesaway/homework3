package appCredit;

import java.util.Scanner;

public class CreditTensors1xSystemOut {
    public static void main(String[] args) {
        System.out.println("Введите имя, возраст, сумму кредита");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        int age = input.nextInt();
        double sumOfCredit = input.nextDouble();
        String badName = "Bob"; //Плохой чувак, ему кредит не давать
        String message = ((!name.equals(badName)) && age >= 18 && sumOfCredit <= age * 100) ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(message);
    }
}